package com.example.projectd.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class GreetingController {
    @GetMapping("/greeting")
    public String hello(){
        return "here goes my greeting";
    }
}
